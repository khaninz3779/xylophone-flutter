import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    List<NoteData> xylophoneNote = [];
    xylophoneNote.add(NoteData(1, Colors.red));
    xylophoneNote.add(NoteData(2, Colors.orange));
    xylophoneNote.add(NoteData(3, Colors.yellow));
    xylophoneNote.add(NoteData(4, Colors.green));
    xylophoneNote.add(NoteData(5, Colors.teal));
    xylophoneNote.add(NoteData(6, Colors.blue));
    xylophoneNote.add(NoteData(7, Colors.purple));

    print(xylophoneNote);

    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              for (var note in xylophoneNote)
                Expanded(
                  child: FlatButton(
                      child: null,
                      onPressed: () {
                        print('note${note.note}.wav');
                        final player = AudioCache();
                        player.play('note${note.note}.wav');
                      },
                      color: note.color,

                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}

class NoteData {
  int note;
  Color color;

  NoteData(this.note, this.color);
}
